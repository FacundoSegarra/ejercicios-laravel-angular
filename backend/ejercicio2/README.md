# Ejercicio 2

## Requerimientos

- Instalar Docker

## Registrar usuario por consola

Pasos para registrar usuario por consola

1- Ingresar
    
```
cd backend/ejercicio2
```

2- Levantar los servicios

```
./vendor/bin/sail up -d
```

3- Ejecutar las migraciones

```
./vendor/bin/sail php artisan migrate
```

4- Ejecutar siguiente comando para registrar un nuevo usuario

```
./vendor/bin/sail php artisan usuario:registrar
```

## Parar y Remover contenedores

```
./vendor/bin/sail down
```