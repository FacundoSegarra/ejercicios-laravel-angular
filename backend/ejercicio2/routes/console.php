<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use App\Models\Usuario;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('usuario:registrar', function () {
    $nombre = $this->ask('Nombre?');
    $email = $this->ask('Email?');

    Usuario::create([
        'nombre' => $nombre,
        'email' => $email,
        'habilitado' => true,
    ]);

    $this->info('Usuario ' . $nombre . ' creado!');
});
