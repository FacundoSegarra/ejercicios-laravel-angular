<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PosicionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Posiciones')->insert([
            ['idTipoProducto' => 6,'fechaEntregaInicio' => '2020-01-20 11:37:40','moneda' => 'USD','precio' => 212.34,'idEmpresa' => 1,'idProducto' => 106],
            ['idTipoProducto' => 1,'fechaEntregaInicio' => '2019-01-05 02:41:53','moneda' => 'USD','precio' => 32.3,'idEmpresa' => 2,'idProducto' => 2],
            ['idTipoProducto' => 2,'fechaEntregaInicio' => '2020-04-12 17:01:12','moneda' => 'USD','precio' => 3232,'idEmpresa' => 4,'idProducto' => 64],
            ['idTipoProducto' => 1,'fechaEntregaInicio' => '2019-12-13 18:17:32','moneda' => 'USD','precio' => 30202,'idEmpresa' => 3,'idProducto' => 12],
            ['idTipoProducto' => 1,'fechaEntregaInicio' => '2020-07-02 23:20:37','moneda' => 'USD','precio' => 4012.3,'idEmpresa' => 4,'idProducto' => 36],
            ['idTipoProducto' => 1,'fechaEntregaInicio' => '2019-10-30 11:37:57','moneda' => 'USD','precio' => 91202,'idEmpresa' => 1,'idProducto' => 12],
            ['idTipoProducto' => 1,'fechaEntregaInicio' => '2020-11-29 13:32:32','moneda' => 'AR$','precio' => 3312.2,'idEmpresa' => 4,'idProducto' => 3],
            ['idTipoProducto' => 1,'fechaEntregaInicio' => '2018-04-25 03:34:32','moneda' => 'AR$','precio' => 10000,'idEmpresa' => 5,'idProducto' => 2],
            ['idTipoProducto' => 5,'fechaEntregaInicio' => '2020-02-01 11:32:40','moneda' => 'AR$','precio' => 30000,'idEmpresa' => 6,'idProducto' => 100],
            ['idTipoProducto' => 1,'fechaEntregaInicio' => '2016-02-28 12:10:12','moneda' => 'AR$','precio' => 15000,'idEmpresa' => 3,'idProducto' => 58]
        ]);
    }
}
