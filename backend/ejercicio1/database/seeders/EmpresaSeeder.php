<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Empresas')->insert([
            ['id' => 1,'cuit' => 30599828431, 'razonSocial' => 'A REGUEIRA Y CIA. S.A. CEREALES'],
            ['id' => 2,'cuit' => 30703597072, 'razonSocial' => 'A Y N SERVICIOS S.R.L.'],
            ['id' => 3,'cuit' => 33541742439, 'razonSocial' => 'A.A.C.R.E.A.'],
            ['id' => 4,'cuit' => 20110058609, 'razonSocial' => 'ABALO, ALBERTO E'],
            ['id' => 5,'cuit' => 30618705672, 'razonSocial' => 'ADECO AGROPECUARIA S.A.'],
            ['id' => 6,'cuit' => 30635322140, 'razonSocial' => 'ESPARTINA S.A.'],
            ['id' => 7,'cuit' => 30539391735, 'razonSocial' => 'CAMPOAMOR HNOS. S.A.C.I.F. Y A.']        
        ]);
    }
}
