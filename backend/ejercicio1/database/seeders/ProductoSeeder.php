<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Productos')->insert([
            ['idTipoProducto' => 1,'id' => 1, 'nombre' => 'TRIGO', 'usoFrecuente' => true],
            ['idTipoProducto' => 1,'id' => 2, 'nombre' => 'MAIZ', 'usoFrecuente' => true],
            ['idTipoProducto' => 1,'id' => 3, 'nombre' => 'LINO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 4, 'nombre' => 'GIRASOL', 'usoFrecuente' => true],
            ['idTipoProducto' => 1,'id' => 5, 'nombre' => 'SORGO', 'usoFrecuente' => true],
            ['idTipoProducto' => 1,'id' => 6, 'nombre' => 'SOJA', 'usoFrecuente' => true],
            ['idTipoProducto' => 1,'id' => 7, 'nombre' => 'AVENA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 8, 'nombre' => 'ALPISTE', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 9, 'nombre' => 'MIJO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 10, 'nombre' => 'CENTENO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 11, 'nombre' => 'CEBADA FORRAJERA', 'usoFrecuente' => true],
            ['idTipoProducto' => 1,'id' => 12, 'nombre' => 'TRIGO CANDEAL', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 13, 'nombre' => 'COLZA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 14, 'nombre' => 'MANI', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 15, 'nombre' => 'ITA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 18, 'nombre' => 'SORGO FORRAJERO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 19, 'nombre' => 'PELLETS DE SOJA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 20, 'nombre' => 'TRIGO SARRACENO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 21, 'nombre' => 'SOJA SUSTENTABLE', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 23, 'nombre' => 'AGROPIRO ALARGADO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 24, 'nombre' => 'ALFALFA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 28, 'nombre' => 'FESTUCA ALTA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 36, 'nombre' => 'SOJA FABRICA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 37, 'nombre' => 'SOJA FABRICA ENTREGA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 39, 'nombre' => 'SEMILLA DE AVENA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 40, 'nombre' => 'SEMILLA DE TRIGO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 41, 'nombre' => 'SEMILLA DE MAIZ', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 42, 'nombre' => 'SEMILLA DE SOJA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 43, 'nombre' => 'SEMILLA DE GIRASOL', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 45, 'nombre' => 'SEMILLA NATURAL GIRASOL', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 46, 'nombre' => 'SEMILLA MACHO GIRASOL', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 49, 'nombre' => 'MAIZ DENTADO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 50, 'nombre' => 'PELLETS DE GIRASOL', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 51, 'nombre' => 'PELLETS DE AFRECHILLO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 52, 'nombre' => 'MOHA CARAPE', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 53, 'nombre' => 'CEBADA CERVECERA', 'usoFrecuente' => true],
            ['idTipoProducto' => 1,'id' => 54, 'nombre' => 'COLZA DOBLE "00"/ CANOLA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 56, 'nombre' => 'MAIZ BLANCO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 58, 'nombre' => 'MANI CAJA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 59, 'nombre' => 'MAIZ ESPECIAL', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 60, 'nombre' => 'MAIZ FLAMENCO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 65, 'nombre' => 'MAIZ PISCINGALLO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 66, 'nombre' => 'MAIZ DURO COLORADO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 67, 'nombre' => 'MAIZ CARGILL 280', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 68, 'nombre' => 'ARROZ', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 69, 'nombre' => 'MANI INDUSTRIA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 70, 'nombre' => 'SOJA CHICAGO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 71, 'nombre' => 'TRIGO CHICAGO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 72, 'nombre' => 'MAIZ CHICAGO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 74, 'nombre' => 'SEMILLA DE CEBADA SCARLETT', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 75, 'nombre' => 'MAIZ PARTIDO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 76, 'nombre' => 'SOJA MINI', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 78, 'nombre' => 'TRIGO MINI', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 79, 'nombre' => 'MAIZ MINI', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 81, 'nombre' => 'MAIZ PARTIDO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 92, 'nombre' => 'CUARTA DE CEBADA', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 95, 'nombre' => 'MINI TRIGO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 96, 'nombre' => 'CT ALGODON', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 97, 'nombre' => 'MAIZ FLINT', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 103, 'nombre' => 'GARBANZO', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 104, 'nombre' => 'POROTO MUNG', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 105, 'nombre' => 'ARVEJA VERDE', 'usoFrecuente' => false],
            ['idTipoProducto' => 1,'id' => 106, 'nombre' => 'EXPELLER', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 47, 'nombre' => 'FOSFATO DIAMONICO', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 48, 'nombre' => 'FOSFATO MONOAMONICO', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 57, 'nombre' => 'GLIFOSATO', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 61, 'nombre' => 'UREA GRANULADA', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 62, 'nombre' => 'UREA PERLADA', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 63, 'nombre' => 'GLIFOSATO ATANOR', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 64, 'nombre' => '2.4 D', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 77, 'nombre' => 'SUPER FOSFATO TRIPLE', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 98, 'nombre' => 'SUPER FOSFATO SIMPLE', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 99, 'nombre' => 'PANZER GOLD', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 200, 'nombre' => 'MEZCLA FISICA 7-40', 'usoFrecuente' => false],
            ['idTipoProducto' => 3,'id' => 90, 'nombre' => 'ACEITE DE SOJA', 'usoFrecuente' => false],
            ['idTipoProducto' => 4,'id' => 80, 'nombre' => 'HARINA SOJA', 'usoFrecuente' => false],
            ['idTipoProducto' => 4,'id' => 91, 'nombre' => 'HARINA DE SOJA HIPRO', 'usoFrecuente' => false],
            ['idTipoProducto' => 5,'id' => 100, 'nombre' => 'DOLAR', 'usoFrecuente' => false],
            ['idTipoProducto' => 5,'id' => 101, 'nombre' => 'DOLAR-MAT', 'usoFrecuente' => false],
            ['idTipoProducto' => 5,'id' => 102, 'nombre' => 'VARIOS', 'usoFrecuente' => false],
            ['idTipoProducto' => 6,'id' => 103, 'nombre' => 'GANADO', 'usoFrecuente' => false],
            ['idTipoProducto' => 2,'id' => 104, 'nombre' => 'GIRASOL ALTO OLEICO', 'usoFrecuente' => true],
            ['idTipoProducto' => 6,'id' => 105, 'nombre' => 'VIENTRES', 'usoFrecuente' => false],
            ['idTipoProducto' => 6,'id' => 106, 'nombre' => 'INVERNADA', 'usoFrecuente' => false],
            ['idTipoProducto' => 6,'id' => 107, 'nombre' => 'GORDO', 'usoFrecuente' => false]
        ]);
    }
}
