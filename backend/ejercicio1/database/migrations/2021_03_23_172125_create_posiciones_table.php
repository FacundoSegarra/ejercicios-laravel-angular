<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Posiciones', function (Blueprint $table) {
            $table->id();
            $table->dateTime('fechaEntregaInicio');
            $table->enum('moneda', ['AR$','USD'])->default('USD');
            $table->double('precio',14,2);

            $table->unsignedBigInteger('idEmpresa');
            $table->foreign('idEmpresa')->references('id')->on('Empresas');

            $table->unsignedBigInteger('idProducto');
            $table->unsignedSmallInteger('idTipoProducto');
            $table->foreign(['idProducto','idTipoProducto'])->references(['id','idTipoProducto'])->on('Productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Posiciones');
    }
}
