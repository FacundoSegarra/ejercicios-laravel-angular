<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Productos', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('nombre',50);
            $table->boolean('usoFrecuente')->default(false);
            // Para simplificar el caso no apunta a ningun lugar el tipo de producto
            $table->unsignedSmallInteger('idTipoProducto');
            $table->primary(['id', 'idTipoProducto']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Productos');
    }
}
