# Ejercicio 1

## Requerimientos

- Docker

## Iniciar la API

Pasos para hacer andar la API

1- Ingresar
    
```
cd backend/ejercicio1
```

2- Instalar dependencias

```
docker run --rm --interactive --tty \
  --volume $PWD:/app \
  composer install
```

3- Copiamos las variables de entorno de ejemplo

```
cp .env.example .env
```

4- Construyo las imagenes de los distintos servicios

```
./vendor/bin/sail build
```

5- Levantar la API

```
./vendor/bin/sail up -d
```

6- Ejecutar las migraciones y cargando datos de prueba en la base de datos

```
./vendor/bin/sail php artisan migrate:fresh --seed
```

7- Ingresando a `http://127.0.0.1/graphql-playground` para probar la API GraphQL

### Consulta
Agregar esto en el cuerpo de la consulta:

```graphql
query {
  posiciones(ordenPorUsoFrecuente: DESC, first: 4, page: 1) {
    paginatorInfo {
      lastItem
      count
      firstItem
      currentPage
      hasMorePages
      perPage
      total
    }
    data {
      id
      fechaEntregaInicio
      moneda
      precio
    }
  }
}
```

### Resultado

```json
{
  "data": {
    "posiciones": {
      "paginatorInfo": {
        "lastItem": 2,
        "count": 2,
        "firstItem": 1,
        "currentPage": 1,
        "hasMorePages": true,
        "perPage": 2,
        "total": 10
      },
      "data": [
        {
          "id": "2",
          "fechaEntregaInicio": "2019-01-05 02:41:53",
          "moneda": "USD",
          "precio": 32.3
        },
        {
          "id": "8",
          "fechaEntregaInicio": "2018-04-25 03:34:32",
          "moneda": "AR$",
          "precio": 10000
        }
      ]
    }
  }
}
```

## Parar contenedores

```
./vendor/bin/sail stop
```

## Parar y remover contenedores

```
./vendor/bin/sail down
```