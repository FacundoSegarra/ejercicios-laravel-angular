<?php

namespace App\GraphQL\Builders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class OrdenarPosicionesPorUsoFrecuenteBuilder
{
    public function apply($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): Builder
    {
        $data = DB::table('Posiciones as pos')
                    ->join('Productos as prod', function ($join) {
                        $join->on('prod.id', '=', 'pos.idProducto')
                            ->on('prod.idTipoProducto', '=', 'pos.idTipoProducto');
                    })
                    ->join('Empresas as emp','emp.id','=','pos.idEmpresa')
                    ->orderBy('prod.usoFrecuente', $args['ordenPorUsoFrecuente'])
                    ->select(
                        'pos.id'
                        ,'pos.fechaEntregaInicio'
                        ,'pos.moneda'
                        ,'pos.precio'
                        ,'emp.razonSocial as empresa'
                        ,'prod.nombre as producto'
                    );

        return $data;

        //return $data->get();
    }
}