export interface IDatosPaginacion {
  cantidadRegistros: number;
  cantidadPaginas: number;
  indicePagina: number;
  paginaPrevia?: number;
}

export interface IQuery {
  posiciones: IDatosPaginacion;
}
export interface IDatosPaginados {
  data: any[];
  paginatorInfo: IPaginatorInfo;
}

export interface IPaginatorInfo {
  count: number;
  currentPage: number;
  firstItem: number;
  hasMorePage: boolean;
  lastItem: number;
  perPage: number;
  total: number;
}
