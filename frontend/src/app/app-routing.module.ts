import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VistasComponent } from './vistas/vistas.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/app/posiciones',
    pathMatch: 'full',
  },
  {
    path: 'app',
    component: VistasComponent,
    loadChildren: () =>
      import('./vistas/vistas.module').then((m) => m.VistasModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
