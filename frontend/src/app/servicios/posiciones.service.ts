import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Apollo, gql } from 'apollo-angular';
import { map } from 'rxjs/operators';

const GET_POSICIONES = gql`
  query GetPosiciones {
    posiciones(
      ordenPorUsoFrecuente: DESC
    ) {
      paginatorInfo {
        lastItem
        count
        firstItem
        currentPage
        hasMorePages
        perPage
        total
      }
      data {
        id
        fechaDisponibilidad: fechaEntregaInicio
        moneda
        precio
        empresa
        producto
      }
    }
  }
`;

@Injectable({
  providedIn: 'root',
})
export class PosicionesService {
  constructor(private apollo: Apollo) {}

  getPosiciones(): Observable<any> {
    return this.apollo
      .watchQuery({
        query: GET_POSICIONES
      })
      .valueChanges.pipe(
        map(({ data }): any => {
          return data;
        })
      );
  }
}
