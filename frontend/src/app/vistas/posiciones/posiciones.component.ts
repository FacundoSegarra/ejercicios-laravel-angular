import { Component, OnInit } from '@angular/core';
import { PosicionesService } from 'src/app/servicios/posiciones.service';
@Component({
  selector: 'app-posiciones',
  templateUrl: './posiciones.component.html',
  styleUrls: ['./posiciones.component.css'],
})
export class PosicionesComponent implements OnInit {
  estaCargando: boolean = true;
  columnas: string[] = [
    'id',
    'empresa',
    'producto',
    'fechaDisponibilidad',
    'precio',
    'moneda',
  ];
  titulosDeColumnas: string[] = [
    'id',
    'empresa',
    'producto',
    'fechaDisponibilidad',
    'precio',
    'moneda',
  ];
  datos: any[];
  cantidadRegistros: number
  cantidadPaginas: number

  constructor(private posicionesService: PosicionesService) {}

  ngOnInit(): void {
    this.getPosiciones();
  }

  getPosiciones() {
    this.estaCargando = true;
    this.posicionesService
      .getPosiciones()
      .subscribe(({ posiciones: { data, paginatorInfo } }) => {
        this.estaCargando = false;
        this.datos = data;
        this.cantidadRegistros = paginatorInfo.total
        this.cantidadPaginas = paginatorInfo.perPage
      });
  }
}
