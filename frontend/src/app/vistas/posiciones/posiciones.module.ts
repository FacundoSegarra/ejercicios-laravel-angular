import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PosicionesRoutingModule } from './posiciones-routing.module';
import { PosicionesComponent } from './posiciones.component';
import {MatTableModule} from '@angular/material/table';
import { TablaModule } from 'src/app/componentes/tabla/tabla.module';
import { CargandoModule } from 'src/app/componentes/cargando/cargando.module';


@NgModule({
  declarations: [
    PosicionesComponent
  ],
  imports: [
    CommonModule,
    PosicionesRoutingModule,
    MatTableModule,
    TablaModule,
    CargandoModule
  ]
})
export class PosicionesModule { }
