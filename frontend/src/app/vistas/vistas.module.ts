import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VistasRoutingModule } from './vistas-routing.module';
import { VistasComponent } from './vistas.component';
import { LayoutModule } from '../layout/layout.module';



@NgModule({
  declarations: [VistasComponent],
  imports: [
    CommonModule,
    VistasRoutingModule,
    LayoutModule,
  ]
})
export class VistasModule { }
