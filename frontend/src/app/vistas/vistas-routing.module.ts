import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404Component } from '../errores/error404/error404.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/app/usuarios',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
  },
  {
    path: 'usuarios',
    loadChildren: () => import('./usuarios/usuarios.module').then(m => m.UsuariosModule),
  },
  {
    path: 'posiciones',
    loadChildren: () => import('./posiciones/posiciones.module').then(m => m.PosicionesModule),
  },
  {
    path: '**', component: Error404Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VistasRoutingModule {}
