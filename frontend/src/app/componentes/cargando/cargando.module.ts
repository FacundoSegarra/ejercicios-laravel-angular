import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CargandoComponent } from './cargando.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [CargandoComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
  ],
  exports: [CargandoComponent]
})
export class CargandoModule { }
