import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaComponent } from './tabla.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [TablaComponent],
  imports: [CommonModule, MatTableModule, MatPaginatorModule],
  exports: [TablaComponent],
})
export class TablaModule {}
