import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css'],
})
export class TablaComponent implements OnInit, AfterViewInit {
  @Input()
  columnas: string[];
  @Input()
  titulosDeColumnas: string[];
  @Input()
  cantidadRegistros: number;
  @Input()
  cantidadPaginas: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Input()
  data: any;

  dataSource: MatTableDataSource<any>;

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.paginator = this.paginator
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}
