# Frontend

## Requerimientos

- Docker
- docker-compose
- Correr el backend del ejercicio 1 para probar

## Iniciar frontend

Pasos para hacer anda

1- Ingresar a esta carpeta pero estan los dos ejercicios juntos
    
```
cd frontend
```

2- Levantar la App

```
docker-compose build && docker-compose up
```

3- Listo ingresando a `http://127.0.0.1:4200` para probar la App!!


## Remover contenedores

```
docker-compose down
```